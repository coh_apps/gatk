ARG SOURCE_DOCKER_REGISTRY=localhost:5000

FROM ${SOURCE_DOCKER_REGISTRY}/ubuntu_opt_gatk:4.1.5.0

ENTRYPOINT ["/opt/gatk/run.sh", "gatk"]
